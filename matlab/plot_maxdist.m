%% Plot the Maximum Distances Between Neighbors Nodes %%

%% CONFIGURE PLOT %%

% save data options
% 0: dont save
% 1: save .pdf
% 2: save .eps
SAVE_OPTIONS = 1;


%% MAIN CODE

% Define maximum distances array
max_dist = zeros(1,N);

for t = 1 : 1 : N
    max_dist(t) = 1e-5;
    
    for i = 1 : n-1
        for j = i+1 : n
            if A(i,j,t) > 0 || A(j,i,t) > 0
                max_dist(t) = max(max_dist(t),norm(x(i,:,t)-x(j,:,t)));
            end
        end
    end
    
    t
end

plot([1:0.5:N/2+0.5],max_dist);
xlim([1 N/2+0.5]);
ylim([15 90]);

set(gca,'FontName','Helvetica');
xlabel('t [s]'); 
ylabel('max(d_{ij}) \forall i,j \in \{1,...,n\}');
grid on;

% save image plot
if SAVE_OPTIONS == 1
    imgname = strcat('maxdist-',int2str(n),'-',int2str(N),'.pdf');
    print('-dpdf',imgname);
elseif SAVE_OPTIONS == 2
    imgname = strcat('maxdist-',int2str(n),'-',int2str(N),'.eps');
    print('-depsc2','-tiff',imgname);
end