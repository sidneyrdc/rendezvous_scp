%% Plot the trajectories of the robots %%

%% CONFIGURE PLOT %%

% save data options
% 0: dont save
% 1: save .pdf
% 2: save .eps
SAVE_OPTIONS = 1;

% plot size in pixels
% 0: auto (do not set both as zero)
% #: set manually
WIDTH = 250;
HEIGHT = 350;

% show communication and security radius
% 0: off
% 1: show
COM_RAD = 0;
SEC_RAD = 1;

% plot line width
LINE_WIDTH = 1.5;

%% MAIN CODE %%

% set the slack to graphic limits
if COM_RAD == 1
    slack = single(max(r_com(:)));
elseif SEC_RAD == 1
    slack = single(max(r_sec(:)));
else
    slack = single(max(r_sec(:))*0.5);
end

% insert a gain to slack
slack = slack*1.1;

xpose = x_data(:, 1, :);
ypose = x_data(:, 2, :);

% get the axis limits
xmin = min(xpose(:));
xmax = max(xpose(:));
ymin = min(ypose(:));
ymax = max(ypose(:));

% insert the slack to x limits
xmin = xmin - slack;
if xmax < 0
    xmax = xmax - slack;
elseif xmax >= 0
    xmax = xmax + slack;
end

% insert the slack to y limits
ymin = ymin - slack;
if ymax < 0
    ymax = ymax - slack;
elseif ymax >= 0
    ymax = ymax + slack;
end

% array of colors
colors = lines(n);%hsv(n);

% calculate the axis ratio according with the specified height and width
if HEIGHT == 0
    % ratio between x and y axis
    AXIS_RATIO = abs(ymin - ymax)/abs(xmin - xmax);

    % define plot height according with its width
    HEIGHT = WIDTH*AXIS_RATIO;
elseif WIDTH == 0
    % ratio between x and y axis
    AXIS_RATIO = abs(xmin - xmax)/abs(ymin - ymax);

    % define plot width according with its height
    WIDTH = HEIGHT*AXIS_RATIO;
end

% set frame resolution
set(gcf, 'MenuBar', 'figure', 'Units', 'pixels', 'Resize', 'off', 'Position', [1, 1, WIDTH, HEIGHT]);

% wait figure to be ready
pause(0.5);

% set axis borders (optimize plot area)
set(gca, 'Units', 'pixels', 'LooseInset', [0, 0, 5, 5]);

newplot;
hold on;

% plot reference position
plot(x_data(n + 1, 1, N), x_data(n + 1, 2, N), 'rx', 'MarkerSize', 15);

% position for the robots
for i = 1 : n
    % plot robot in the position (x, y)
    plot(x_data(i, 1, N), x_data(i, 2, N), 'Marker', '.', 'Color', colors(i, :), 'MarkerSize', 20);

    x_plot = x_data(i, 1, 1:N);
    y_plot = x_data(i, 2, 1:N);

    % show agents path
    plot(x_plot(:), y_plot(:), 'Color', colors(i, :), 'LineStyle', '-', 'LineWidth', LINE_WIDTH);

    % show range of communication
    if COM_RAD == 1
        viscircles(x_data(i, 1:2, N), r_com(i), 'EdgeColor', 'k', 'LineWidth', 0.2, 'LineStyle', '-.');
    end

    % show coverage range
    if SEC_RAD == 1
        viscircles(x_data(i, 1:2, N), r_sec(i), 'EdgeColor', 'k', 'LineWidth', 0.5, 'LineStyle', '-.');
    end

    % show number of each agent
    text(x_data(i, 1, 1), x_data(i, 2, 1), strcat('(', num2str(i), ')'));

    if i == n
        xr_plot = x_data(n + 1, 1, 1:N);
        yr_plot = x_data(n + 1, 2, 1:N);

        plot(xr_plot(:), yr_plot(:), 'Color', 'k', 'LineStyle', '--', 'LineWidth', 1);
    end
end

xlabel('x (m)');
ylabel('y (m)');

% set axis configurations
axis([xmin xmax ymin ymax]);
axis equal manual;

grid on;
box on;
hold off;

% configure text font and size (use 'listfonts' to list all known fonts)
set(findall(gcf, '-property', 'FontName'), 'FontName', 'Times New Roman')
set(findall(gcf, '-property', 'FontSize'), 'FontSize', 12)

pause(2)

% get current picture
fig = gcf;

% figure position [left, bottom, width, height]
set(fig, 'Units', 'Inches');
pos = get(fig, 'Position');
set(fig, 'PaperPositionMode', 'Auto', 'PaperUnits', 'Inches', 'PaperSize', [pos(3), pos(4)]);

% save image plot
if SAVE_OPTIONS == 1
    imgname = strcat('traj-',int2str(n),'-',int2str(N),'.pdf');
    print('-dpdf', '-r0', imgname);
elseif SAVE_OPTIONS == 2
    imgname = strcat('traj-',int2str(n),'-',int2str(N),'.eps');
    print('-depsc2','-tiff',imgname);
end

