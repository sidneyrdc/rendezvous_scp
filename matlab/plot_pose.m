%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Robot's Position Plotter
%
% Maintainer: Sidney Carvalho - sydney.rdc@gmail.com
% Last Change: 2017 Dec 04 02:05:32
% Info: This code is able to plot the robot's positions from the topology
% control algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Runnable Options %%

% size of communication arrows
ARROW_LENGTH = 2;

% size of communications links
LINE_WIDTH = 1;

% set plot width
WIDTH = 250;

% save plot options
% 0: don't save
% 1: save .pdf
% 2: save .eps
% 3: save .avi
SAVE_OPTIONS = 3;

% video speed
VID_SPEED = 4;

% time for each iteration
LOOP_TIME = 0;

% show communication and coverage radii
% 0: off
% 1: show
COM_RAD = 0;
SEC_RAD = 1;

% show velocity directions
% 0: off
% 1: show
VEL_DIR = 1;

%% Main Code %%

% clear all figure options
close all
clf('reset')

% disable warnings
warning('off', 'all');

% set the slack to graphic limits
if COM_RAD == 1
    slack = single(max(r_com(:)));
elseif SEC_RAD == 1
    slack = single(max(r_sec(:)));
else
    slack = single(max(r_sec(:))*0.5);
end

% insert a gain to slack
slack = slack*1.1;

xpose = x_data(:, 1, :);
ypose = x_data(:, 2, :);

% get the axis limits
xmin = min(xpose(:));
xmax = max(xpose(:));
ymin = min(ypose(:));
ymax = max(ypose(:));

% insert the slack to x limits
xmin = xmin - slack;
if xmax < 0
    xmax = xmax - slack;
elseif xmax >= 0
    xmax = xmax + slack;
end

% insert the slack to y limits
ymin = ymin - slack;
if ymax < 0
    ymax = ymax - slack;
elseif ymax >= 0
    ymax = ymax + slack;
end

% ration between x and y axis
AXIS_RATIO = abs(ymin - ymax)/abs(xmin - xmax);

% define plot height according with its width
HEIGHT = WIDTH*AXIS_RATIO;

% video writer
if SAVE_OPTIONS == 3
    % set videowriter properties
    mov = VideoWriter('sim');
    mov.Quality = 100;
    mov.FrameRate = VID_SPEED*double(1/h);
    open(mov);

    % indicator 1st frame got
    got_frame = false;

    % set frame resolution
    set(gcf, 'MenuBar', 'none', 'Units', 'pixels', 'Resize', 'off', 'Position', [1, 1, WIDTH, HEIGHT]);

    % wait figure to be ready
    pause(0.5);

    % set axis borders (optimize plot area)
    set(gca, 'Units', 'pixels', 'LooseInset', [0, 0, 5, 5]);
end

% set the object sizes according with the plot size
ARROW_LENGTH = 0.04*norm([xmin ymin] - [xmax ymax]);
ROBOT_LENGTH = 0.06*norm([xmin ymin] - [xmax ymax]);
SPEED_LENGTH = 0.03*norm([xmin ymin] - [xmax ymax]);

% start position plotting for robots
for t = 1 : 1 : N
    newplot;
    hold on;

    % print the references
    for i = n + 1 : size(x_data, 1)
        plot(x_data(i, 1, t), x_data(i, 2, t), 'rx', 'MarkerSize', 15);
    end

    % show line communication between neighbors agents
    for i = 1 : n - 1
        for j = i + 1 : n
            % get the position for each robot
            xi = x_data(i, 1 : 2, t);
            xj = x_data(j, 1 : 2, t);

            % euclidian distance between i and j
            dij = norm(xi - xj);

            if A_data(i, j, t) == 1 && A_data(j, i, t) == 1
                % plot a continuous line when the link between i and j is real
                line([xi(1) xj(1)], [xi(2) xj(2)], 'LineStyle', '-', 'Linewidth', LINE_WIDTH, 'Color', 'b');

            elseif A_data(i, j, t) == 1 && A_data(j, i, t) == 0
                % plot a continuous arrow from j to i
                draw_line2([xj(1) xj(2)], [xi(1) xi(2)], 'LineStyle', '-', 'LineColor', 'm', 'ArrowColor', 'm', 'ArrowEdgeColor', 'm', 'ArrowLength', ARROW_LENGTH, 'LineWidth', LINE_WIDTH);

            elseif A_data(i, j, t) == 0 && A_data(j, i, t) == 1
                % plot a continuous arrow from i to j
                draw_line2([xi(1) xi(2)], [xj(1) xj(2)], 'LineStyle', '-', 'LineColor', 'm', 'ArrowColor', 'm', 'ArrowEdgeColor', 'm', 'ArrowLength', ARROW_LENGTH, 'LineWidth', LINE_WIDTH);

            end
        end
    end

    for i = 1 : n
        % get the position for each robot i
        xi = x_data(i, 1 : 2, t);

        % show velocities directions
        if VEL_DIR == 1
            if t < N
                % get the velocity unity vector
                xi1 = xi + ROBOT_LENGTH*(x_data(i, 1 : 2, t + 1) - xi)/norm(x_data(i, 1 : 2, t + 1) - xi);

                % plot the velocity unity vector arrow
                if not(isnan(xi1))
                    draw_line2(xi, xi1, 'ArrowLength', SPEED_LENGTH, 'ArrowAngle', 20, 'ArrowEdgeColor', 'r', 'LineColor', 'r', 'LineWidth', 2.5);
                end
            end
        end

        % show communication range
        if COM_RAD == 1
            uistack(viscircles(xi, r_com(i), 'EdgeColor', 'k', 'LineWidth', 0.1, 'LineStyle', '-.'), 'bottom');
        end

        % show coverage range
        if SEC_RAD == 1
            uistack(viscircles(xi, r_sec(i), 'EdgeColor', 'k', 'LineWidth', 0.1, 'LineStyle', '-.'), 'bottom');
        end

        % plot the agent's path
        xpose = x_data(i, 1, 1 : t);
        ypose = x_data(i, 2, 1 : t);
        uistack(plot(xpose(:), ypose(:), 'g-'), 'bottom');

        % plot the robot's name and shape
        draw_robot(xi, x_data(i, 3, t), ROBOT_LENGTH, 'RobotShape', 'c', 'RobotLabel', num2str(i), 'FillColor', 'w');

    end

    % configure text font and size (use 'listfonts' to list all known fonts)
    set(findall(gcf, '-property', 'FontName'), 'FontName', 'Times New Roman')
    set(findall(gcf, '-property', 'FontSize'), 'FontSize', 12)

    % set axis labels
    xlabel('x (m)');
    ylabel('y (m)');

    % set axis configurations
    axis([xmin xmax ymin ymax]);

    % set axis behaviour according with the save mode
    if SAVE_OPTIONS == 3
        axis manual normal;
    else
        axis equal manual;
    end

    % enable box around the figure
    box on;

    % get picture frame
    getframe(gcf);

    % save file options
    if SAVE_OPTIONS == 1
        % set print size
        fig = gcf;
        fig.PaperSize = [3 3];

        imgname = strcat('test2_pose-', int2str(t));
        print(fig, '-dpdf', '-fillpage', imgname);

    % save eps
    elseif SAVE_OPTIONS == 2
        imgname = strcat('pose-', int2str(t));
        print('-depsc2', '-tiff', imgname);

    % save avi
    elseif SAVE_OPTIONS == 3
        % insert information of video speed
        text(-0.2, 8.1, strcat('x', int2str(VID_SPEED)), 'Color', 'w', 'BackgroundColor', [.7 .9 .7], 'FontSize', 14, 'FontWeight', 'bold');

        % get axis handle
        ax = gca;

        if got_frame == false
            % set the region of interest
            rect = [-ax.TightInset(1), -ax.TightInset(2), ax.Position(3)+ax.TightInset(1)+ax.TightInset(3)+5, ax.Position(4)+ax.TightInset(2)+ax.TightInset(4)+5];

            % set got_frame as true to avoid redefinition of the interest area
            % (matlab changes the axis causing error in the video writing)
            got_frame = true;
        end

        % get picture frame only to the interest area
        frame = getframe(ax, rect);

        % write frame in video file
        writeVideo(mov, frame);
    end

    % enable picture update
    hold off;

    % print time iteration
    t

    % wait for LOOP_TIME
    pause(LOOP_TIME)
end

% close movie file
if SAVE_OPTIONS == 3
    close(mov);
end

