%% Plot the trajectories of the robots in the time %%

%% CONFIGURE PLOT %%

% save data options
% 0: dont save
% 1: save .pdf
% 2: save .eps
SAVE_OPTIONS = 0;

% plot line width
LINE_WIDTH = 1;

% plot markers size (use 0 to off)
MARKER_SIZE = 0;

%% MAIN CODE %%

% array of colors
colors = hsv(double(n));

% array of markers
markers = ['^','d','*','o','x','s','.','^','v','>','<','p','h'];

% label names
names = '';

% clear picture labels
clear botpl;

for i = 1 : n
    % plot the x coordinates for all robots
    subplot(2,1,1);
    hold on;

    x_plot = x(i,1,:);

    if MARKER_SIZE == 0
        botpl(i) = plot(h:h:N*h+h,x_plot(:),'Col',colors(i,:),'LineWidth',LINE_WIDTH);
    else
        botpl(i) = plot(h:h:N*h+h,x_plot(:),'Marker',markers(i),'MarkerSize',MARKER_SIZE,'LineWidth',LINE_WIDTH);
    end

    xlabel('t [s]');
    ylabel('x [m]');
    xlim([h N*h]);

    names{i} = strcat('(',int2str(i),')');

    if i == n
        % plot reference coordinate at x axis
        %refpl = plot(h:h:N*h+h,repmat(x(n+1,1,1),1,size(x,3)),'Col','k','LineWidth',1,'LineStyle','--');
        x_ref = x(n+1, 1, :);
        refpl = plot(h:h:N*h+h, x_ref(:), 'Col', 'k', 'LineWidth', 1, 'LineStyle', '--');

        % plot labels
        names{n+1} = 'r';
        legend([botpl,refpl],names{:},'Location','southeast');
        grid on;
        box on;
    end

    % plot the y coordinates for all robots
    subplot(2,1,2)
    hold on;

    y_plot = x(i,2,:);

    if MARKER_SIZE == 0
        plot(h:h:N*h+h,y_plot(:),'Col',colors(i,:),'LineWidth',LINE_WIDTH);
    else
        plot(h:h:N*h+h,y_plot(:),'Marker',markers(i),'MarkerSize',MARKER_SIZE,'LineWidth',LINE_WIDTH);
    end

    % plot reference coordinate at y axis
    if i == n
        %plot(h:h:N*h+h,repmat(x(n+1,2,1),1,size(x,3)),'Col','k','LineWidth',1,'LineStyle','--');
        y_ref = x(n+1, 2, :);
        refpl = plot(h:h:N*h+h, y_ref(:), 'Col', 'k', 'LineWidth', 1, 'LineStyle', '--');
    end

    xlabel('t [s]');
    ylabel('y [m]');
    xlim([h N*h]);
    grid on;
    box on;
end

% save image plot
if SAVE_OPTIONS == 1
    imgname = strcat('trajtime-',int2str(n),'-',int2str(N),'.pdf');
    print('-dpdf',imgname);
elseif SAVE_OPTIONS == 2
    imgname = strcat('trajtime-',int2str(n),'-',int2str(N),'.eps');
    print('-depsc2','-tiff',imgname);
end

