#!/usr/bin/env julia

# simulation prefix
SIM = 2

# robots number
n = 6

# iteration number
N = 70

# initial positions
# x(i,[x|y|θ],t]
x = zeros(n + 1, 3, N + 1)
x[1, 1:2, 1] = [3 1]
x[2, 1:2, 1] = [6 5]
x[3, 1:2, 1] = [5.9 3]
x[4, 1:2, 1] = [6 0]
x[5, 1:2, 1] = [6 -3]
x[6, 1:2, 1] = [6.1 -5]

# initial velocities
# v(i,[x|y],t]
v = zeros(n + 1, 2, N + 1)

# reference point
x[7, 1:2, 1] = [10 0]

# topology adjacency matrix
# A(i,j,t]
A = zeros(n, n + 1, N)

# knowledge about reference
A[1, n + 1, 1] = 1

# topology type
# 0: fixed topology
# 1: dynamic topology
TOP = 1

# ROS environment (provides access to real robots)
# 0: off
# 1: on
ROS = 0

# names of the robots in ROS
rosnames = ["robot_0",
            "robot_1",
            "robot_2",
            "robot_3",
            "robot_4",
            "robot_5"]

# types of the robots in ROS
# 1: rosaria
# 2: stage/gazebo/vrep
# 3: turtlesim
rostypes = [2, 2, 2, 2, 2, 2]

# reference dynamics
# 0: null dynamics (fixed position and velocity)
# 1: non-linear dynamics using sine
RD = 0

# neighbour's position linear prediction
# 0: there is no prediction to the neighbour positions
# 1: there is prediction to the neighbour positions using linear extrapolation
NLP = 1

# communication constraint
# 0: off
# 1: on
COM = 1

# collision avoidance constraint
# 0: off
# 1: on
SEC = 1

# integration step
h = 0.1

# prediction horizon
p = 30

# penalization to velocity variation
gdv = 10

# position gain
gx = 1.2
gxr = 100

# velocity gain
gv = 0
gvr = 0

# saturation of the control signal
vmax = 1
vmin = -1

# maximum tries to find a feasible point
smax = 10

# SCP security radius
rho = 1

# communication and security radius
rcom = [7 9 5 5 4 4]
rsec = ones(1, n)*0.5

# robots settled as non mobile (obstacles)
nonmobile = []

# call the rendezvous algorithm
include("rendezvous_scp.jl")

