#= Rendezvous Algorithm with SCP and MPC =#

# load external modules
using MAT       # to save .mat files
using Convex    # cvx interface to solve convex problems
using Gurobi    # to use Gurobi as optimization solver
using Cxx       # to wrapper C++ code (demands Julia v0.6 or later)

# load Cxx environment if ROS interface is used
if ROS == 1
    # default C++ lib and include paths
    const path_to_lib = "../lib/"
    const path_to_include = "../include/"

    # define the path_to_include as default C++ headers folder
    addHeaderDir(path_to_include, kind=C_System)

    # load ROS interface shared library
    Libdl.dlopen(path_to_lib * "libros_interface.so", Libdl.RTLD_GLOBAL)

    cxxinclude("ros_interface.hpp")                 # ROS interface header
    cxxinclude("vector")                            # C++ vector header

    # instantiate a 'ros_interface' object
    ros_com = @cxxnew ros_interface(pointer("rendezvous_scp"))

    # position and velocity arrays on ROS
    ros_pose = @cxxnew space_t()
    ros_vel = @cxxnew space_t()

    # add robots to ROS environment
    for i = 1 : n
        # set robot initial position at ROS
        @cxx ros_pose->set_x(x[i, 1, 1])
        @cxx ros_pose->set_y(x[i, 2, 1])

        # add the robot to the ROS environment
        @cxx ros_com->add_node(i, rostypes[i], pointer(rosnames[i]), ros_pose)
    end

    # control saturation (low level control)
    const MAX_VA = 1                  # maximum angular velocity

    # dead zones (low level control)
    const MAX_DA = pi/7               # maximum angular variation
    const MIN_DA = 0.01               # minimum angular variation

    # variables of proportionality (low level control)
    ka = 0                            # to angular control

    # initialize iteration time
    iter_dt = 0
end

# dimension of state and control variables
const m = 2

# constant matrices to motion control
const T = kron(tril(ones(p, p)), eye(m))
const TI = inv(T)
const I = eye(m*p)
const T2 = T - I

# normalization terms of MPC
const MAX_DX = gx./rcom
const MAX_DXR = gxr/maximum([norm(x[i, 1:2, 1] - x[n + 1, 1:2, 1]) for i in find(A[:, n + 1, 1])])
const MAX_DV = gv/norm([vmin vmax], Inf)
const MAX_DDV = gdv/norm([vmin vmax], Inf)
const MAX_DVR = gvr/norm([vmin vmax], Inf)

# start counting time
tic()

# main loop
for t = 1 : N
    println("iter -> $(t)")

    if ROS == 1
        # calculate the ROS sampling time according with the passed time
        ros_dt = h - iter_dt

        # check if h is enough to do all the processing
        if ros_dt < 0
            println("WARNING@rendezvous_scp.jl \tSampling time is too small! It has to be greater than $(h - ros_dt) seconds.")

            # nullify the ROS sampling time
            ros_dt = 0
        end

        # set the data capture frequency (s)
        @cxx ros_com->clock(ros_dt)

        # get data from ROS
        for i = 1 : n
            # get robot position from ROS
            ros_pose = @cxx ros_com->node_pose(i)

            # set current robot position with ROS received data
            x[i, 1, t] = @cxx ros_pose->x
            x[i, 2, t] = @cxx ros_pose->y
            x[i, 3, t] = @cxx ros_pose->yaw
        end

        # set the low level control signals
        vx = zeros(n)
        va = zeros(n)

        # start measuring iteration time
        t0 = @cxx ros_com->get_time()
    end

    # generate the reference trajectory (velocity)
    if RD == 1
        v[n + 1, :, t + 1] = [0.75 sin(0.05*t + pi)]
    else
        v[n + 1, :, t + 1] = [0 0]
    end

    # upgrade the reference pose
    x[n + 1, 1:2, t + 1] = x[n + 1, 1:2, t] + h*v[n + 1, 1:2, t + 1]

    # communication loop
    for i = 1 : n
        # definition of auxiliary matrices
        Hi = zeros(m*p, m*p)                # MPC Hessian matrix
        Gi = zeros(1, m*p)                  # MPC gradient vector
        Xi = repmat(x[i, 1:2, t], p, 1)
        Vi0 = zeros(m*p, 1)
        Vi0[1:2] = v[i, :, t]

        for j = 1 : n + 1
            # adjacency matrix update rule
            if TOP == 1 && j != n + 1 && j != i
                # euclidean norm between i and j
                dij = norm(x[i, 1:2, t] - x[j, 1:2, t])

                # update adjacency matrix
                if dij <= rcom[j]
                    A[i, j, t] = 1
                elseif dij > rcom[j]
                    A[i, j, t] = 0
                end
            elseif (TOP == 0 || j == n + 1) && t > 1
                A[i, j, t] = A[i, j, t - 1]
            end

            # auxiliary matrices definition
            Xj = repmat(x[j, 1:2, t], p, 1)
            Vj = repmat(v[j, 1:2, t], p, 1)

            # fill auxiliary matrices for all neighbors of i
            if j != n + 1 && !contains(==, nonmobile, j)
                Hi += (T'*h^2*MAX_DX[j]*T + MAX_DV*I)*A[i, j, t]

                # check if the neighbour linear prediction is enabled
                if NLP == 1
                    Gi += ((Xi - Xj)'*h*MAX_DX[j]*T - (T2*Vj)'*h^2*MAX_DX[j]*T - Vj'*MAX_DV)*A[i, j, t]
                else
                    Gi += ((Xi - Xj)'*h*MAX_DX[j]*T - Vj'*MAX_DV)*A[i, j, t]
                end

            # MPC term when j is the reference
            elseif j == n + 1
                Hi += (T'*h^2*MAX_DXR*T + MAX_DVR*I)*A[i, j, t]
                Gi += ((Xi - Xj)'*h*MAX_DXR*T - Vj'*MAX_DVR)*A[i, j, t]
            end
        end

        # fill hessian and gradient matrices
        Hi += TI'*MAX_DDV*TI
        Gi -= Vi0'*MAX_DDV*TI

        # scp start point
        vs = v[i, :, t]

        # neighbors of i
        Ni = find(A[i, :, t])

        # solve consensus problem for each robot
        for s = 1 : smax

            # if the robots are marked as non mobile or have no neighbours,
            # don't calculate their control signal
            if contains(==, nonmobile, i) || isempty(Ni)
                vs = [0, 0]
                break
            end

            # declare optimization problem variables
            U = Convex.Variable(m*p)

            # objective function
            problem = minimize(
                0.5*quadform(U, Hi) + Gi*U,
                # saturation constraints
                U >= fill(vmin, m*p),
                U <= fill(vmax, m*p),
            )

            for j in Ni
                if j != n + 1
                    if !contains(==, nonmobile, j) && COM == 1
                        # maximum distance between neighbours constraint
                        problem.constraints += norm(x[i, 1:2, t] + U[1:2]*h - x[j, 1:2, t]) <= rcom[j]
                    end

                    if SEC == 1
                        # minimum distance constraint
                        problem.constraints += x[i, 1:2, t]'*x[i, 1:2, t] + 2*x[i, 1:2, t]'*h*vs - 2*x[i, 1:2, t]'*x[j, 1:2, t] +
                                            - 2*x[j, 1:2, t]'*h*vs + h^2*vs'*vs + x[j, 1:2, t]'*x[j, 1:2, t] + (2*x[i, 1:2, t]'*h +
                                            - 2*x[j, 1:2, t]'*h + 2*h^2*vs')*(U[1:2] - vs) >= (rsec[i] + rsec[j])^2
                    end
                end
            end

            # limit the new velocity to be inside the security zone
            problem.constraints += norm(vs - U[1:2], 1) <= rho

            # solve the optimization problem
            solve!(problem, GurobiSolver(OutputFlag = 0))

            # verify if the solution unfeasible
            if sum(isnan.(U.value)) > 0
                # set the velocities as null if the maximum SCP step was reached
                if s == smax
                    vs = [0, 0]

                # if not, start with a feasible random point
                else
                    vs = rand(vmin:0.001:vmax, 2)
                end

            # verify if the solution is close enough to the last SCP step
            elseif norm(vs - U.value[1:2]) <= 0.001*norm(U.value[1:2])
                vs = U.value[1:2]
                break

            # update the current control action of the SCP step
            else
                vs = U.value[1:2]
            end
        end

        if ROS == 1 && !contains(==, nonmobile, i)
            # get the reference angle (the angle that the line initiating in the current
            # point and finishing in the reference gets with the frame x axis)
            # check the quadrants and hold the reference angle between (-π, π)
            if vs[1] == 0 && vs[2] == 0
                ra = x[i, 3, t]
            elseif vs[1] >= 0 && vs[2] >= 0                   # 1st quadrant
                ra = atan(vs[2]/vs[1])
            elseif vs[1] <= 0 && vs[2] >= 0                   # 2nd quadrant
                ra = pi - atan(abs(vs[2]/vs[1]))
            elseif vs[1] <= 0 && vs[2] <= 0                   # 3rd quadrant
                ra = atan(abs(vs[2]/vs[1])) - pi
            elseif vs[1] >= 0 && vs[2] <= 0                   # 4th quadrant
                ra = atan(vs[2]/vs[1])
            end

            # calculate the angular variation
            da = ra - x[i, 3, t]

            # get the shorter arc and with opposite signal (to perform the best movement)
            da > pi ? da = da - 2*pi : da < -pi ? da = 2*pi - abs(da) : 0

            # calculate the angular control gain
            if da > -MIN_DA && da < MIN_DA
                ka = 0
            elseif da > MAX_DA
                ka = 1
            elseif da < -MAX_DA
                ka = -1
            elseif abs(da) >= MIN_DA && abs(da) <= MAX_DA
                ka = da/MAX_DA
            end

            # calculate the angular control signal
            va[i] = MAX_VA*ka

            # get linear variation
            dx = norm(vs)

            # calculate the linear control signal
            vx[i] = dx*(1 - abs(ka))

        else
            # integrate the velocities
            x[i, 1:2, t + 1] = x[i, 1:2, t]' + h*vs'
        end

        # store the control signals
        v[i, :, t + 1] = vs'
    end

    if ROS == 1
        # exit if ros_interface is not running
        if !@cxx ros_com->ros_ok()
            exit(1)
        end

        # send the generated velocities to ROS
        for i = 1 : n
            # set robot velocity
            @cxx ros_vel->set_x(vx[i])
            @cxx ros_vel->set_yaw(va[i])

            # send velocities to ROS
            @cxx ros_com->node_vel(i, ros_vel)
        end

        # call ros_interface shutdown
        if t == N
            @cxx ros_com->shutdown()
        end

        # get the iteration elapsed time (from ROS viewpoint)
        iter_dt = (@cxx ros_com->get_time()) - t0
    end
end

# count the real simulation elapsed time and print it in the screen
runtime = toc()

#
#=
 = Save simulation data
 =#

# create .mat file
file = matopen(join(["../matlab/sim=", SIM, "-n=", n, "-N=", N, "-p=", p, "-gx=", gx,
                     "-gxr=", gxr, "-gv=", gv, "-gvr=", gvr, "-gdv=", gdv, "-rd=", RD,
                     "-h=", h, "-com=", COM, "-sec=", SEC, "-ros=", ROS, "-nlp=", NLP,
                     "-top=", TOP, ".mat"]), "w")

# write data into .mat file
write(file, "runtime", Float64(runtime))
write(file, "N", Float32(N))
write(file, "n", Float32(n))
write(file, "h", Float32(h))
write(file, "p", Int32(p))
write(file, "x_data", x)
write(file, "v_data", v)
write(file, "vmax", vmax)
write(file, "vmin", vmin)
write(file, "r_com", rcom)
write(file, "r_sec", rsec)
write(file, "A_data", A)

# close .mat file
close(file)

