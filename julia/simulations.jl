#= GENERAL SIMULATIONS JCAES2017 =#

#===============================================================================
 = simulation parameters
 ==============================================================================#

# simulations number
NSIM = 3

# simulation control parameters
# 0: all
# 1: spread robots
# 2: avoiding the wall
# 3: trajectory tracking
SIM = 3

# reference dynamics
# 0: null dynamics (fixed position and velocity)
# 1: non-linear dynamics using sine
RD = 0

# topology type
# 0: fixed topology
# 1: dynamic topology
TOP = 1

# integration step
h = 1

# prediction horizon
p = 5

# penalization to velocity variation
gi = 1

# position gain
pg = 1

# reference knowledge gain
rg = 1e3

# velocity gain
vg = 6

# reference velocity gain
vr = 0

# robots number
n = 6

# iteration number
N = 200

# saturation of the control signal
vmax = 1
vmin = -1

# maximum tries to SCP finds a feasible point
kmax = 30

# SCP trust region factor
rho = 1

# communication and security radius
rcom = [70, 90, 50, 50, 30, 30]
rsec = ones(1,n) * 5

#===============================================================================
 = simulation code
 ==============================================================================#

# initial positions
# x([i | r], [x | y], t)
x = zeros(n + 1, 2, N + 1)

# initial velocities
# v([i | r], [x | y], t)
v = zeros(n + 1, 2, N + 1)

# topology adjacency matrix
# A(i, [j | r], t)
A = zeros(n, n + 1, N + 1)

# spread robots
if SIM == 1
    # initial positions
    x[1, :, 1] = [60 85]
    x[2, :, 1] = [100 155]
    x[3, :, 1] = [145 170]
    x[4, :, 1] = [40 50]
    x[5, :, 1] = [70 30]
    x[6, :, 1] = [185 140]

    # reference point
    x[7, :, 1] = [120 100]

    # knowledge about reference
    #=A[1, n + 1, 1] = rg=#
    A[3, n + 1, 1] = rg

# avoid the wall
elseif SIM == 2
    # initial positions
    x[1, :, 1] = [30 10]
    x[2, :, 1] = [60 50]
    x[3, :, 1] = [59 30]
    x[4, :, 1] = [60 0]
    x[5, :, 1] = [60 -30]
    x[6, :, 1] = [61 -50]

    # reference point
    x[7, :, 1] = [100 0]

    # knowledge about reference
    A[1, n + 1, 1] = rg

# trajectory tracking
elseif SIM == 3
    # initial positions
    x[1, :, 1] = [30 10]
    x[2, :, 1] = [60 50]
    x[3, :, 1] = [59 30]
    x[4, :, 1] = [80 0]
    x[5, :, 1] = [50 -5]
    x[6, :, 1] = [61 -50]

    # reference point
    x[7, :, 1] = [100 0]

    # knowledge about reference
    A[1, n + 1, 1] = rg
    A[4, n + 1, 1] = rg

    # reference dynamics
    RD = 1
end

# call the rendezvous algorithm
include("rendezvous_scp.jl")

